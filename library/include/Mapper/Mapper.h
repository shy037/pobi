#ifndef MAPPER_H
#define MAPPER_H

#include "Map.h"

class Mapper
{
private:
	Map_ptr map;

public:
	Mapper(int, int);
	~Mapper();
	void drawMe();
	int checkMyPlace(MapObject_ptr);
	void updateMap(MapObject_ptr, bool);
	void updateSchneik(Schneik_ptr);
	std::vector<std::vector<int>> getGrid();
	Map_ptr getMap();
	std::string saveMap();
};

#endif
