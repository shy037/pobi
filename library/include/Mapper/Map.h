#ifndef MAP_H
#define MAP_H

#include "SchneikManager/Schneik.h"

class Map
{
private:
	int mapHeight;
	int mapWidth;
    std::vector<std::vector<int>> grid;

public:
	Map(int, int);
	~Map();
	int getWidth();
	int getHeight();
	std::vector<std::vector<int>> getGrid();
	void addToGrid(MapObject_ptr);
	void popFromGrid(MapObject_ptr);
};
typedef std::shared_ptr<Map> Map_ptr;

#endif
