#ifndef SUPERVISOR_H
#define SUPERVISOR_H

#include "EdiblesManager.h"
#include "Mapper.h"
#include "SchneikManager.h"
#include <memory>
#include <iostream>

class Supervisor
{
private:
	Mapper mapper;
	EdiblesManager ediblesManager;
	SchneikManager schneikManager;
	bool run;
	int score;
public:
	Supervisor(int, int);
	Supervisor(int, int, int, SchneikObject_ptr, std::vector<SchneikObject_ptr>, int, int, int, int, std::vector<std::vector<Edible_ptr>>, int, int, int);
	~Supervisor();
	//Glowna petla gry
	void theGame();
	//Metody odpowiedzialne za wczytanie/zapis gry
	void loadTheGame();
	void saveTheGame();
	//Metoda odpowiedzialna za zbalansowanie i dodanie do mapy objektow Edible.
	void updateEdibles(Map_ptr);
	//Metody odpowiedzialne za poruszanie/zmianę kierunku/przekazanie Schneik'a do mapy.
	bool moveTheSchneik();
	void changeDirectionOfSchneik(char);
	void pushSchneikToMap(Schneik_ptr);
	//Metody obslugujace Mapper'a, odpowiadajace za dodanie/usuniecie elementu z mapy,
	//sprawdzenie co znajduje sie na danym miejscu(uzywane dla glowy Schneika, po wykonaniu ruchu),
	//oraz do wypisania grid'u na konsole
	void pushToMap(MapObject_ptr);
	void popFromMap(MapObject_ptr);
	int whereAmI(MapObject_ptr);
	void showMe();
};
#endif
