#ifndef SCHNEIK_NULLPTREXCEPTION_H
#define SCHNEIK_NULLPTREXCEPTION_H

#include <stdexcept>
#include <iostream>

class NullptrException : public std::logic_error
{
public:
    const static std::string msg1;

    explicit NullptrException (const std::string& );

};


#endif //SCHNEIK_NULLPTREXCEPTION_H
