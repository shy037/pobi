#ifndef SCHNEIK_FILEEXCEPTION_H
#define SCHNEIK_FILEEXCEPTION_H

#include <stdexcept>
#include <iostream>

class FileException : public std::logic_error
{
public:
    const static std::string msg1;

    explicit FileException (const std::string& );
};


#endif //SCHNEIK_FILEEXCEPTION_H
