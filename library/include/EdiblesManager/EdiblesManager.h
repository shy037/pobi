#ifndef EDIBLES_MANAGER_H
#define EDIBLES_MANAGER_H

#include <vector>
#include "Edible.h"
#include "Map.h"

class EdiblesManager
{
private:
	int negativeAmount;
	int positiveAmount;
	int waterAmount;
	int poisonAmount;
	std::vector<std::vector<Edible_ptr>> overview;

public:
	EdiblesManager();
	EdiblesManager(int,int,int,int,std::vector<std::vector<Edible_ptr>>);
	~EdiblesManager();
	std::vector<Edible_ptr> balanceAmount(Map_ptr);
	Edible_ptr addNewPos(Map_ptr);
	Edible_ptr addNewNeg(Map_ptr);
	Edible_ptr addNewWater(Map_ptr);
	Edible_ptr addNewPoison(Map_ptr);
	bool delWater(int, int);
	bool delFood(int, int);
	std::string saveEdibles();
	Edible_ptr getEdible(int, int);


};

#endif
