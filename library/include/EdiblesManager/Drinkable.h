#ifndef WATER_H
#define WATER_H

#include "Edible.h"
#include "DrinkableType.h"

class Drinkable : public Edible
{
private:
	int hydrationValue;
    DrinkableType type;

public:
	Drinkable(int, int, int);
	~Drinkable();
	int getBaseValue();
	int getType();

};
typedef std::shared_ptr<Drinkable> Drinkable_ptr;

#endif
