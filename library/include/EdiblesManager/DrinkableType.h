#ifndef DRINKABLETYPE_H
#define DRINKABLETYPE_H


typedef enum{
    Poison = -1,
    Water = 1
} DrinkableType;

#endif
