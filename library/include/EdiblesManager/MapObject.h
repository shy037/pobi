#ifndef MAP_OBJECT_H
#define MAP_OBJECT_H

#include <memory>

class MapObject
{
private:
	int x;
	int y;
	int category;

public:
	MapObject(int, int, int);
	virtual ~MapObject();
	int getX();
	int getY();
	int getCat();

	void setX(int);
	void setY(int);
};

typedef std::shared_ptr<MapObject> MapObject_ptr;
#endif
