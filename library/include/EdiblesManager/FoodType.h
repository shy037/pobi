#ifndef FOOD_TYPE_H
#define FOOD_TYPE_H

typedef enum{
	SpoiledFood = -1,
	LeanFood = 1,
	NormalFood = 2,
	FatFood = 3
} FoodType;
#endif
