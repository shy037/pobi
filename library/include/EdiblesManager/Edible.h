#ifndef EDIBLE_H
#define EDIBLE_H

#include "MapObject.h"

class Edible : public MapObject
{
public:
	Edible(int, int, int);
	virtual ~Edible();

	virtual int getBaseValue()=0;

};

typedef std::shared_ptr<Edible> Edible_ptr;
#endif
