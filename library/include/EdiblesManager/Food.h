#ifndef FOOD_H
#define FOOD_H

#include "Edible.h"
#include "FoodType.h"

class Food : public Edible
{
private:
	int calorieValue;
	FoodType type;

public:
	Food(int, int, int);
	~Food();
	int getBaseValue();
	int getType();


};
typedef std::shared_ptr<Food> Food_ptr;
#endif
