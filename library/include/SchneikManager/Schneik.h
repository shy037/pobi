#ifndef SCHNEIK_H
#define SCHNEIK_H

#include <vector>

#include "SchneikManager/SchneikObject.h"

class Schneik
{
private:
	SchneikObject_ptr head;
	std::vector<SchneikObject_ptr> tail;
	int calories;
	int direction;
	int hydration;

public:
	Schneik(SchneikObject_ptr, std::vector<SchneikObject_ptr>, int, int);
	Schneik(SchneikObject_ptr, std::vector<SchneikObject_ptr>, int, int, int);
	~Schneik();
	int getCalories();
	int getHydration();
	int getDirection();
	SchneikObject_ptr getHead();
	std::vector<SchneikObject_ptr> getTail();
	void changeTail(SchneikObject_ptr, int);
	void extendTail(int, int);

	void setDirection(int);
	void addCalories(int);
	void addHydration(int);

	void tickSchneik();
};
typedef std::shared_ptr<Schneik> Schneik_ptr;
#endif
