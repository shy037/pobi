#ifndef SCHNEIK_MANAGER_H
#define SCHNEIK_MANAGER_H

#include "Schneik.h"
#include "Map.h"

class Supervisor;

class SchneikManager
{
private:
	Schneik_ptr schneiken;

public:
	SchneikManager(int);
	SchneikManager(SchneikObject_ptr, std::vector<SchneikObject_ptr>, int, int, int);
    ~SchneikManager();
	SchneikObject_ptr getHead();
	void changeDirection(char);
	void changeTailOrder();
	bool didIDieCrawling();
	void tickSchneik();
	Schneik_ptr getSchneik();

	std::string saveSchneik();

};
#endif
