#ifndef SCHNEIK_OBJECT_H
#define SCHNEIK_OBJECT_H


#include "MapObject.h"

class SchneikObject : public MapObject
{
public:
	SchneikObject(int, int, int);
	~SchneikObject();

};
typedef std::shared_ptr<SchneikObject> SchneikObject_ptr;

#endif
