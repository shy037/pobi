#include <time.h>
#include <fstream>
#include "Supervisor.h"
#include "Food.h"
#include "Drinkable.h"

int main(){
    srand (time(NULL));
    char choice;
    std::cout << "Press 'o' to open a saved file\nPress 'n' to start new game\n";
    std::cin >> choice;
    if(choice =='o') {
        std::ifstream save;
        save.open("save.txt");
        int dir, cal, hyd, length;
        save >> dir >> cal >> hyd >> length;
        int x, y, ctg;
        save >> x >> y >> ctg;
        SchneikObject_ptr hd(new SchneikObject(x,y,ctg));
        std::vector<SchneikObject_ptr> tl;
        for(int i = 0; i < length; i++){
            save >> x >> y >> ctg;
            SchneikObject_ptr tmp(new SchneikObject(x,y,ctg));
            tl.push_back(tmp);
        }
        int pos, neg, wat, poi;
        save >> pos >> neg >> wat >> poi;
        std::vector<std::vector<Edible_ptr>> edibles;
        std::vector<Edible_ptr> tmp;
        for(int i = 0; i < 4; i++){
            tmp.clear();
            edibles.push_back(tmp);
        }
        for(int i = 0; i < pos; i++){
            save >> x >> y >> ctg;
            Food_ptr tmp(new Food(x,y,ctg));
            edibles[0].push_back(tmp);
        }
        for(int i = 0; i < neg; i++){
            save >> x >> y >> ctg;
            Food_ptr tmp(new Food(x,y,ctg));
            edibles[1].push_back(tmp);
        }
        for(int i = 0; i < wat; i++){
            save >> x >> y >> ctg;
            Drinkable_ptr tmp(new Drinkable(x,y,ctg));
            edibles[2].push_back(tmp);
        }
        for(int i = 0; i < poi; i++){
            save >> x >> y >> ctg;
            Drinkable_ptr tmp(new Drinkable(x,y,ctg));
            edibles[3].push_back(tmp);
        }
        int wid, hei, sc;
        save >> wid >> hei;
        save >> sc;
        save.close();
        Supervisor sup(cal, hyd,  dir, hd, tl, pos, neg, wat, poi, edibles, wid, hei, sc);
        sup.theGame();
    }
    else if(choice == 'n'){
        Supervisor sup(10,10);
        sup.theGame();
    }


    return 0;
}