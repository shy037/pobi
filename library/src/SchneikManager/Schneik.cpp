#include "Schneik.h"
#include "NullptrException.h"


Schneik::Schneik(SchneikObject_ptr hd, std::vector<SchneikObject_ptr> tl, int newCal, int newHyd)
: head(hd), tail(tl), calories(newCal), hydration(newHyd)
{
    direction = 2;
}

Schneik::Schneik(SchneikObject_ptr hd, std::vector<SchneikObject_ptr> tl, int newCal, int newHyd, int dir)
		: head(hd), tail(tl), calories(newCal), hydration(newHyd), direction(dir)
{
}

Schneik::~Schneik()
{
}

int Schneik::getCalories()
{
	return calories;
}

int Schneik::getDirection()
{
	return direction;
}

int Schneik::getHydration() {
	return hydration;
}

SchneikObject_ptr Schneik::getHead() {
	return head;
}

std::vector<SchneikObject_ptr> Schneik::getTail() {
	return tail;
}
void Schneik::setDirection(int newDir)
{
	direction = newDir;
}
void Schneik::changeTail(SchneikObject_ptr obj, int pos) {
	if(obj == nullptr)
		throw(NullptrException(NullptrException::msg1));
	else {
		tail[pos]->setX(obj->getX());
		tail[pos]->setY(obj->getY());
	}
}

void Schneik::addCalories(int newCal)
{
    calories += newCal;
}

void Schneik::addHydration(int newHyd) {
	hydration += newHyd;
}

void Schneik::tickSchneik(){
	hydration-=50;
	calories-=10;
}
void Schneik::extendTail(int x, int y) {
	SchneikObject_ptr obj(new SchneikObject(x,y,1));
	tail.push_back(obj);
}

