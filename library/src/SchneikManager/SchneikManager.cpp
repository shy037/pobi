#include "SchneikManager.h"
#include <sstream>

SchneikManager::SchneikManager(int h)
{
	SchneikObject_ptr hd(new SchneikObject(4, (h-1)/2, 2));
	SchneikObject_ptr tl1(new SchneikObject(3, (h-1)/2, 1));
	SchneikObject_ptr tl2(new SchneikObject(2, (h-1)/2, 1));
	std::vector<SchneikObject_ptr> tl;
	tl.push_back(tl1);
	tl.push_back(tl2);
	Schneik_ptr tmp = std::make_shared<Schneik>(hd,tl,200,2000);
	schneiken = tmp;
}

SchneikManager::SchneikManager(SchneikObject_ptr hd, std::vector<SchneikObject_ptr> tl, int cal, int hyd, int dir)
: schneiken(std::make_shared<Schneik>(hd, tl, cal, hyd, dir))
{
}

SchneikManager::~SchneikManager() = default;

Schneik_ptr SchneikManager::getSchneik() {
    return schneiken;
}

void SchneikManager::changeDirection(char newDir) {
	switch(newDir) {
		case 'w': {
			if (schneiken->getDirection() != 3)
				schneiken->setDirection(1);
			break;
		}
		case 'd': {
			if (schneiken->getDirection() != 4)
				schneiken->setDirection(2);
			break;
		}
		case 's': {
			if (schneiken->getDirection() != 1)
				schneiken->setDirection(3);
			break;
		}
		case 'a': {
			if (schneiken->getDirection() != 2)
				schneiken->setDirection(4);
			break;
		}
	}
}




void SchneikManager::changeTailOrder() {
	int tailSize = schneiken->getTail().size();

	for(int i = tailSize-1; i > 0; i--){
		if(schneiken->getTail()[i]!=schneiken->getTail()[i-1]);
			schneiken->changeTail(schneiken->getTail()[i-1], i);
	};
	schneiken->changeTail(getHead(), 0);


}

bool SchneikManager::didIDieCrawling()
{
	this->tickSchneik();
	if(schneiken->getCalories() < 0 or schneiken->getHydration() < 0)
		return true;
	return false;
}

void SchneikManager::tickSchneik() {
	schneiken->tickSchneik();
}

SchneikObject_ptr SchneikManager::getHead() {
	return schneiken->getHead();
}

std::string SchneikManager::saveSchneik() {
	std::stringstream tmp;
	tmp << schneiken->getDirection() << " " << schneiken->getCalories() << " " << schneiken->getHydration() << " "<< schneiken->getTail().size() << '\n';
	tmp << schneiken->getHead()->getX() << " " << schneiken->getHead()->getY() << " " << schneiken->getHead()->getCat() << "\n";
	for(int i= 0; i < schneiken->getTail().size(); i++)
			tmp << schneiken->getTail()[i]->getX() << " " << schneiken->getTail()[i]->getY() << " " << schneiken->getTail()[i]->getCat() << "\n";

	return tmp.str();
}
