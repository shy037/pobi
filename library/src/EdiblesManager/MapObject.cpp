#include "MapObject.h"

MapObject::MapObject(int x, int y, int ctg)
        : x(x), y(y), category(ctg)
{
}

MapObject::~MapObject() = default;

int MapObject::getX()
{
    return x;
}

int MapObject::getY()
{
    return y;
}

int MapObject::getCat()
{
    return category;
}

void MapObject::setX(int newX)
{
    x = newX;
}

void MapObject::setY(int newY)
{
    y = newY;
}
