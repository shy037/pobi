#include <Map.h>
#include <Food.h>
#include "Drinkable.h"
#include "EdiblesManager.h"
#include <sstream>

EdiblesManager::EdiblesManager()
{
    positiveAmount = 6;
    negativeAmount = 3;
    waterAmount = 2;
    poisonAmount = 1;
    std::vector<Edible_ptr> tmp;
    for(int i = 0; i < 4; i++){
        tmp.clear();
        overview.push_back(tmp);
    }
}

EdiblesManager::EdiblesManager(int pos,int neg,int wat,int poi,std::vector<std::vector<Edible_ptr>> edibles)
:   positiveAmount(pos),negativeAmount(neg),waterAmount(wat),poisonAmount(poi),overview(edibles)
{}


EdiblesManager::~EdiblesManager() = default;

std::string EdiblesManager::saveEdibles() {
    std::stringstream tmp;
    tmp<<positiveAmount<<" "<<negativeAmount<<" "<<waterAmount<<" "<<poisonAmount<<'\n';
    for(int i= 0; i < overview.size(); i++)
        for(int j = 0; j < overview[i].size(); j++){
            tmp << overview[i][j]->getX()<<" "<<overview[i][j]->getY()<<" "<<overview[i][j]->getCat()<<'\n';
        }
    return tmp.str();
}



std::vector<Edible_ptr> EdiblesManager::balanceAmount(Map_ptr map)
{
    std::vector<Edible_ptr> changedEdibles;
    while(overview[0].size() < positiveAmount){
        changedEdibles.push_back(addNewPos(map));
    }
    while(overview[1].size() < negativeAmount){
        changedEdibles.push_back(addNewNeg(map));
    }
    while(overview[2].size() < waterAmount){
        changedEdibles.push_back(addNewWater(map));
    }
    while(overview[3].size() < poisonAmount){
        changedEdibles.push_back(addNewPoison(map));
    }
    return changedEdibles;
}

Edible_ptr EdiblesManager::addNewPos(Map_ptr map)
{
    int ctg = (rand() % 3)+3;
    int x = rand() % map->getWidth();
    int y = rand() % map->getHeight();
    while(map->getGrid()[x][y] != 0){
        x = rand() % map->getWidth();
        y = rand() % map->getHeight();
    }
    Food_ptr food(new Food(x, y, ctg));

    overview[0].push_back(food);
    return food;
}

Edible_ptr EdiblesManager::addNewNeg(Map_ptr map)
{
    int x = rand() % map->getWidth();
    int y = rand() % map->getHeight();
    while(map->getGrid()[x][y] != 0){
        x = rand() % map->getWidth();
        y = rand() % map->getHeight();
    }
    Food_ptr food(new Food(x, y, 6));
    overview[1].push_back(food);
    return food;
}

Edible_ptr EdiblesManager::addNewWater(Map_ptr map)
{
    int x = rand() % map->getWidth();
    int y = rand() % map->getHeight();
    while(map->getGrid()[x][y] != 0){
        x = rand() % map->getWidth();
        y = rand() % map->getHeight();
    }
    Drinkable_ptr water(new Drinkable(x, y, 7));
    overview[2].push_back(water);
    return water;
}

Edible_ptr EdiblesManager::addNewPoison(Map_ptr map)
{
    int x = rand() % map->getWidth();
    int y = rand() % map->getHeight();
    while(map->getGrid()[x][y] != 0){
        x = rand() % map->getWidth();
        y = rand() % map->getHeight();
    }
    Drinkable_ptr poison(new Drinkable(x, y, 8));
    overview[3].push_back(poison);
    return poison;
}

bool EdiblesManager::delWater(int x, int y)
{
    bool flag = false;
    for(int i = 2; i < 4; i++) {
        for (int j = 0; j < overview[i].size(); j++) {
            if ((x == overview[i][j]->getX()) and (y == overview[i][j]->getY())) {
                flag = true;
                overview[i].erase(overview[i].begin() + j);
                break;
            }
        }
    }
    return flag;
}

bool EdiblesManager::delFood(int x, int y)
{
    bool flag = false;
    for(int i = 0; i < 2; i++)
        for (int j = 0; j < overview[i].size(); j++)
            if ((x == overview[i][j]->getX()) and (y == overview[i][j]->getY())) {
                flag = true;
                overview[i].erase(overview[i].begin() + j);
                break;
            }
    return flag;
}

Edible_ptr EdiblesManager::getEdible(int x, int y) {
    Edible_ptr tmp;
    for(int i = 0; i < 4; i++)
        for (int j = 0; j < overview[i].size(); j++)
            if ((x == overview[i][j]->getX()) and (y == overview[i][j]->getY())) {
                tmp = overview[i][j];
                break;
            }
    return tmp;
}
