#include "Drinkable.h"

Drinkable::Drinkable(int x, int y, int ctg)
		: Edible(x, y, ctg)
{
	switch (ctg){
		case 7:
			type = Water;
			break;
		case 8:
			type = Poison;
			break;
	}
	hydrationValue = 500;
}


Drinkable::~Drinkable(){}

int Drinkable::getBaseValue()
{
	return hydrationValue*type;
}
