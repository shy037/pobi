#include "Food.h"


Food::Food(int x, int y, int ctg)
        : Edible(x, y, ctg)
{
    calorieValue = 60;
    switch (ctg){
        case 3:
            type = LeanFood;
            break;
        case 4:
            type = NormalFood;
            break;
        case 5:
            type = FatFood;
            break;
        case 6:
            type = SpoiledFood;
    }
}

Food::~Food()
{
}

int Food::getBaseValue()
{
    return calorieValue*type;
}
