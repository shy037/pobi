#include "Mapper.h"
#include "NullptrException.h"
#include <iostream>
#include <sstream>

Mapper::Mapper(int width, int height)
{
	Map_ptr tmp = std::make_shared<Map>(width,height);
	map = tmp;
}

Mapper::~Mapper() = default;

void Mapper::updateSchneik(Schneik_ptr schneiken) {
	if(schneiken == nullptr)
		throw(NullptrException(NullptrException::msg1));
	else {
		updateMap(schneiken->getHead(), true);
		int tmp = schneiken->getTail().size();
		std::vector<SchneikObject_ptr> tmpVec = schneiken->getTail();
		for (int i = 0; i < tmp; i++) {
			updateMap(tmpVec[i], true);
		}
	}
}

void Mapper::drawMe()
{
	for(int i = 0; i < map->getWidth(); i++){
		for(int j = 0; j < map->getHeight(); j++)
			std::cout << map->getGrid()[i][j] << " ";
		std::cout << '\n';
	}
}



int Mapper::checkMyPlace(MapObject_ptr sth)
{
	return map->getGrid()[sth->getY()][sth->getX()];
}

void Mapper::updateMap(MapObject_ptr obj, bool flag){
	if(obj == nullptr)
		throw(NullptrException(NullptrException::msg1));
	else {
		if (flag)
			map->addToGrid(obj);
		else
			map->popFromGrid(obj);
	}
}


std::vector<std::vector<int>> Mapper::getGrid(){
	return map->getGrid();
}

Map_ptr Mapper::getMap() {
	return map;
}

std::string Mapper::saveMap() {
	std::stringstream tmp;
	tmp << map->getWidth()<<" "<<map->getHeight()<<'\n';
	return tmp.str();
}