#include "Map.h"
#include "NullptrException.h"


Map::Map(int width, int height)
:  mapWidth(width), mapHeight(height)
{
	std::vector<int> tmp;
	for(int i = 0; i < mapWidth; i++){
		tmp.clear();
		for(int j = 0; j < mapHeight; j++){
            if((i == 0) or (j == 0) or (i == mapWidth-1) or (j == mapHeight-1))
                tmp.push_back(-1);
            else
                tmp.push_back(0);
		}
		grid.push_back(tmp);
	}
}

Map::~Map() = default;

int Map::getWidth()
{
	return mapWidth;
}

int Map::getHeight()
{
	return mapHeight;
}

std::vector<std::vector<int>> Map::getGrid(){
    return grid;
}

void Map::addToGrid(MapObject_ptr obj) {
	if(obj == nullptr)
		throw(NullptrException(NullptrException::msg1));
	else
		grid[obj->getY()][obj->getX()] = obj->getCat();
}

void Map::popFromGrid(MapObject_ptr obj) {
	if(obj == nullptr)
		throw(NullptrException(NullptrException::msg1));
	else
		grid[obj->getY()][obj->getX()] = 0;
}