#include "Supervisor.h"
#include "Food.h"
#include "Drinkable.h"
#include "NullptrException.h"
#include "FileException.h"
#include <conio.h>
#include <fstream>
#include <string>


Supervisor::Supervisor(int width, int height)
:   schneikManager(SchneikManager(height)), ediblesManager(EdiblesManager()), mapper(Mapper(width,height))
{
    run = true;
    score = 0;
    this->pushSchneikToMap(schneikManager.getSchneik());
}

Supervisor::Supervisor(int cal, int hyd, int dir, SchneikObject_ptr hd, std::vector<SchneikObject_ptr> tl, int pos, int neg, int wat, int poi, std::vector<std::vector<Edible_ptr>> edibles, int wid, int hei, int sc)
:schneikManager(SchneikManager(hd, tl,cal,hyd,dir)), ediblesManager(EdiblesManager(pos,neg,wat,poi,edibles)),mapper(Mapper(wid,hei)),score(sc)
{
    run = true;
    for(int i = 0; i < edibles.size(); i++)
        for (int j = 0; j < edibles[i].size(); j++)
            this->pushToMap(edibles[i][j]);
    this->pushSchneikToMap(schneikManager.getSchneik());
}

void Supervisor::saveTheGame() {

    std::ofstream save;

    if(!save.good())
        throw(FileException(FileException::msg1));
    else {
        save.open("save.txt");
        save << schneikManager.saveSchneik();
        save << ediblesManager.saveEdibles();
        save << mapper.saveMap();
        save.close();
    }

}

Supervisor::~Supervisor() = default;

void Supervisor::pushSchneikToMap(Schneik_ptr schneiken) {
    mapper.updateSchneik(schneiken);
}

void Supervisor::changeDirectionOfSchneik(char key) {
    if(key == 't')
        this->saveTheGame();
    else
        schneikManager.changeDirection(key);
}

void Supervisor::theGame(){
    while(run) {

        this->updateEdibles(mapper.getMap());

        if (kbhit()) {
            this->changeDirectionOfSchneik(getch());
        }

        if(!this->moveTheSchneik()) {
            run = !run;
            _sleep(3000);
        }

        std::cout << "\nCalories: " << schneikManager.getSchneik()->getCalories();
        std::cout << "\nHydration: " << schneikManager.getSchneik()->getHydration();
        _sleep(1000);
        system("cls");
    }
}

bool Supervisor::moveTheSchneik(){

    if(schneikManager.didIDieCrawling()){
        std::cout << "Game over\nCalories and Hydration are important\n";
        return false;
    }
    int x = schneikManager.getSchneik()->getHead()->getX();
    int y = schneikManager.getSchneik()->getHead()->getY();
    this->popFromMap(schneikManager.getSchneik()->getHead());

    int tailSize = schneikManager.getSchneik()->getTail().size();
    for(int i = 0; i < tailSize; i++){
        this->popFromMap(schneikManager.getSchneik()->getTail()[i]);
    }
    int tailX = schneikManager.getSchneik()->getTail()[tailSize-1]->getX();
    int tailY = schneikManager.getSchneik()->getTail()[tailSize-1]->getY();
    schneikManager.changeTailOrder();

    for(int i = 0; i < tailSize; i++)
        this->pushToMap(schneikManager.getSchneik()->getTail()[i]);

    int direction = schneikManager.getSchneik()->getDirection();

    switch (direction) {
        case 1: {
            x += 0;
            y += -1;
            break;
        }
        case 2: {
            x += 1;
            y += 0;
            break;
        }
        case 3: {
            x += 0;
            y += 1;
            break;
        }
        case 4: {
            x += -1;
            y += 0;
            break;
        }
        default:
            std::cout << direction;
            std::cout << "Directions fault, SchneikManager\n";
            break;
    }
    schneikManager.getSchneik()->getHead()->setX(x);
    schneikManager.getSchneik()->getHead()->setY(y);

    int place = this->whereAmI(schneikManager.getSchneik()->getHead());

    this->pushToMap(schneikManager.getSchneik()->getHead());

    this->showMe();


    if(place == 0)
        return true;
    if(place == 1 or place == -1) {
        std::cout << "Game over\n";
        return false;
    }
    if(place < 6 and place > 2){
        schneikManager.getSchneik()->extendTail(tailX,tailY);
        score++;
        std::cout << "You got a point! Score : " << score << '\n';
    }
    if(place == 6) {
        score--;
        std::cout << "You ate spoiled food! That's not a way to Schneik like a pro! Score : " << score << '\n';
    }
    if(place < 7 and place > 2){
        Edible_ptr tmp = ediblesManager.getEdible(x, y);
        this->popFromMap(tmp);
        ediblesManager.delFood(x, y);
        schneikManager.getSchneik()->addCalories(tmp->getBaseValue());
        std::cout << "\nYou ate something! It was worth: " << tmp->getBaseValue();
        return true;
    }
    if(place == 7 or place == 8) {
        Edible_ptr tmp = ediblesManager.getEdible(x, y);
        this->popFromMap(tmp);
        ediblesManager.delWater(x, y);
        schneikManager.getSchneik()->addHydration(tmp->getBaseValue());
        std::cout << "\nYou drunk something! It was worth: " << tmp->getBaseValue();
        return true;
    }
    return false;
}

int Supervisor::whereAmI(MapObject_ptr obj) {
    return mapper.checkMyPlace(obj);
}

void Supervisor::pushToMap(MapObject_ptr sth){
    if(sth == nullptr)
        throw(NullptrException(NullptrException::msg1));
    else {
        mapper.updateMap(sth, true);
    }
}

void Supervisor::popFromMap(MapObject_ptr sth){
    if(sth == nullptr)
        throw(NullptrException(NullptrException::msg1));
    else {
        mapper.updateMap(sth, false);
    }
}

void Supervisor::showMe() {
    mapper.drawMe();
}

void Supervisor::updateEdibles(Map_ptr map) {
    if(map == nullptr)
        throw(NullptrException(NullptrException::msg1));
    else {
        std::vector<Edible_ptr> changedEdibles = ediblesManager.balanceAmount(map);
        int sizeOfVec = changedEdibles.size();
        for (int i = 0; i < sizeOfVec; i++)
            pushToMap(changedEdibles[i]);
    }
}

