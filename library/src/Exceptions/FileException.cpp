#include "FileException.h"

const std::string FileException::msg1 = "File error (it may not exist)";

FileException::FileException(const std::string& what_arg)
        : logic_error(what_arg) {}
