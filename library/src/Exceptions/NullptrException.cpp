#include "NullptrException.h"

const std::string NullptrException::msg1 = "Pointer is null";

NullptrException::NullptrException(const std::string& what_arg)
        : logic_error(what_arg) {}
