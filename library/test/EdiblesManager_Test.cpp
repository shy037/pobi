#include <boost/test/unit_test.hpp>
#include <EdiblesManager.h>
#include <Map.h>

BOOST_AUTO_TEST_SUITE(EdiblesManagerTest)

    BOOST_AUTO_TEST_CASE(DeleteEdible) {
        std::shared_ptr<EdiblesManager> ediblesManager = std::make_shared<EdiblesManager>();
        std::shared_ptr<Map> map = std::make_shared<Map>(5, 5);

        Edible_ptr neg = ediblesManager->addNewNeg(map);

        BOOST_REQUIRE_EQUAL(ediblesManager->delWater(neg->getX(), neg->getY()), false);
        BOOST_REQUIRE_EQUAL(ediblesManager->delFood(neg->getX(), neg->getY()), true);

        Edible_ptr pos = ediblesManager->addNewPos(map);

        BOOST_REQUIRE_EQUAL(ediblesManager->delWater(pos->getX(), pos->getY()), false);
        BOOST_REQUIRE_EQUAL(ediblesManager->delFood(pos->getX(), pos->getY()), true);

        Edible_ptr wat = ediblesManager->addNewWater(map);

        BOOST_REQUIRE_EQUAL(ediblesManager->delFood(wat->getX(), wat->getY()), false);
        BOOST_REQUIRE_EQUAL(ediblesManager->delWater(wat->getX(), wat->getY()), true);



    }

    BOOST_AUTO_TEST_SUITE_END()
