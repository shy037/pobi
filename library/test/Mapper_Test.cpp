#include <boost/test/unit_test.hpp>
#include <Mapper.h>
#include <Map.h>
#include <Food.h>

BOOST_AUTO_TEST_SUITE(MapperTest)

    BOOST_AUTO_TEST_CASE(DeleteEdible) {
        std::shared_ptr<Mapper> mapper = std::make_shared<Mapper>(10, 10);

        std::shared_ptr<Food> food1 = std::make_shared<Food>(1, 2, 4);
        std::shared_ptr<Food> food2 = std::make_shared<Food>(3, 4, 5);
        std::shared_ptr<Food> food3 = std::make_shared<Food>(5, 6, 6);

        BOOST_REQUIRE_EQUAL(food1->getX(), 1);
        BOOST_REQUIRE_EQUAL(food1->getY(), 2);
        BOOST_REQUIRE_EQUAL(food1->getCat(), 4);

        BOOST_REQUIRE_EQUAL(food2->getX(), 3);
        BOOST_REQUIRE_EQUAL(food2->getY(), 4);
        BOOST_REQUIRE_EQUAL(food2->getCat(), 5);

        BOOST_REQUIRE_EQUAL(food3->getX(), 5);
        BOOST_REQUIRE_EQUAL(food3->getY(), 6);
        BOOST_REQUIRE_EQUAL(food3->getCat(), 6);


        mapper->getMap()->addToGrid(food1);
        mapper->getMap()->addToGrid(food2);
        mapper->getMap()->addToGrid(food3);

        BOOST_REQUIRE_EQUAL(mapper->checkMyPlace(food1), 4);
        BOOST_REQUIRE_EQUAL(mapper->checkMyPlace(food2), 5);
        BOOST_REQUIRE_EQUAL(mapper->checkMyPlace(food3), 6);

    }

BOOST_AUTO_TEST_SUITE_END()