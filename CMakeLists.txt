cmake_minimum_required(VERSION 3.10)

#Configuration parameters of cmake
set(CMAKE_CXX_COMPILER g++)#requires g++ to be available on PATH
set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(Schneik)
enable_language(CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_VERBOSE_MAKEFILE TRUE)

#Source files here
set(SOURCE_FILES
        library/src/main.cpp
        library/src/Supervisor.cpp
        library/src/SchneikManager/Schneik.cpp
        library/src/SchneikManager/SchneikManager.cpp
        library/src/SchneikManager/SchneikObject.cpp
        library/src/Mapper/Map.cpp
        library/src/Mapper/Mapper.cpp
        library/src/Mapper/Mapper.cpp
        library/src/EdiblesManager/Edible.cpp
        library/src/EdiblesManager/EdiblesManager.cpp
        library/src/EdiblesManager/Food.cpp
        library/src/EdiblesManager/MapObject.cpp
        library/src/EdiblesManager/Drinkable.cpp
        library/src/Exceptions/NullptrException.cpp
        library/src/Exceptions/FileException.cpp
        )

add_executable(Schneik ${SOURCE_FILES})

target_include_directories(Schneik PUBLIC
        library/include
        library/include/SchneikManager
        library/include/Mapper
        library/include/EdiblesManager
        library/include/Exceptions)

find_package(Boost 1.65 COMPONENTS "unit_test_framework" REQUIRED)

include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}/include
        ${Boost_INCLUDE_DIRS}
)
#Test source files here
set(SOURCE_TEST_FILES
        library/src/Supervisor.cpp
        library/src/SchneikManager/Schneik.cpp
        library/src/SchneikManager/SchneikManager.cpp
        library/src/SchneikManager/SchneikObject.cpp
        library/src/Mapper/Map.cpp
        library/src/Mapper/Mapper.cpp
        library/src/Mapper/Mapper.cpp
        library/src/EdiblesManager/Edible.cpp
        library/src/EdiblesManager/EdiblesManager.cpp
        library/src/EdiblesManager/Food.cpp
        library/src/EdiblesManager/MapObject.cpp
        library/src/EdiblesManager/Drinkable.cpp
        library/test/master.cpp
        library/test/EdiblesManager_Test.cpp
        library/test/Mapper_Test.cpp
        library/test/Supervisor_Test.cpp
        )

add_executable(TestSchneik ${SOURCE_TEST_FILES})

target_include_directories(TestSchneik PUBLIC
        library/include
        library/include/SchneikManager
        library/include/Mapper
        library/include/EdiblesManager)

target_link_libraries(TestSchneik
        ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})

enable_testing()

add_test(NAME Test COMMAND TestSchneik
        --report_level=detailed
        --log_level=all
        --color_output=yes)

add_custom_target(check ${CMAKE_COMMAND} -E env CTEST_OUTPUT_ON_FAILURE=1 BOOST_TEST_LOG_LEVEL=all
        ${CMAKE_CTEST_COMMAND} -C $<CONFIG> --verbose
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR})